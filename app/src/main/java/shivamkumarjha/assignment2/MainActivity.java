package shivamkumarjha.assignment2;

import android.app.NotificationManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inf = getMenuInflater();
        inf.inflate(R.menu.menu,menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId())
        {
            case R.id.a:
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Dialog Title!")
                        .setMessage("Dialog demo message!")
                        .show();
                return true;
            case R.id.b:
                NotificationCompat.Builder ncb = new NotificationCompat.Builder(MainActivity.this);
                ncb.setSmallIcon(R.mipmap.ic_launcher);
                ncb.setContentText("Notification panel demo.");
                ncb.setContentTitle(getResources().getString(R.string.app_name));
                ncb.setSubText("Notification subtext.");
                ncb.setTicker("Ticker");
                NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                nm.notify(0,ncb.build());
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
